package solver

import solver.dsl.SATSolver
import solver.dsl.SATSolver._

object test extends App {

  SATSolver { solver =>
    val v0 = solver.int("un", 0, 2)
    val v1 = solver.int("deux", 7, 9)

    v0 === v1 and v1 === v0

    solver.onSolved {
      println("FINIS")
      println(solver.solution)
    }

    solver.onInfeasible {
      println("IMPOSSIBLE")
    }

    solver.solve
  }

}
package solver.examples.dsl

import solver.dsl.SATSolver
import solver.dsl.SATSolver._

object DSLcoloring extends App {

  SATSolver {
    solver =>
      val nNodes = 10
      val Nodes = 0 until nNodes
      var maxColor = 10

      val colors = solver.rangeInt(Nodes)("node ", 0, maxColor)

      val nColors = solver int ("ncolor", 0, maxColor)
      val rand = new scala.util.Random(0)

      for (n1 <- Nodes; n2 <- Nodes; if n1 < n2 && rand.nextInt(100) < 70)
        colors(n1) !== colors(n2)

      for (n <- Nodes)
        nColors >== colors(n)

      solver onSolved {
        println(solver.solution)
      }

      while (solver solveWith (maxColor >>> nColors))
        maxColor = nColors.value(solver.solution)

      println("finished")

  }

}
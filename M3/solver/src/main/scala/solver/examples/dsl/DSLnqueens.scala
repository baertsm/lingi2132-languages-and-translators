package solver.examples.dsl

import solver.dsl.SATSolver
import solver.dsl.SATSolver._

object DSLnqueens extends App {

  SATSolver {
    solver =>
      val nQueens = 10
      val queens = 0 until nQueens

      val columns = solver.rangeInt(queens)("queen", 1, nQueens)

      val upDiags = queens.map(i => columns(i) - i)
      val doDiags = queens.map(i => columns(i) + i)

      allDifferents(columns: _*)
      allDifferents(upDiags: _*)
      allDifferents(doDiags: _*)

      solver onSolved {
        println(solver.solution)
      }
      solver onInfeasible {
        println("infeasible")
      }

      solver solve
  }
}


package solver.examples.dsl

import solver.expressions.Sum
import solver.dsl.SATSolver
import solver.dsl.SATSolver._

object DSLknapsack extends App {

  SATSolver {
    solver =>
      val nItems = 5
      val items = 0 until nItems

      val profits = Array(2, 5, 1, 3, 4)
      val weights = Array(3, 4, 2, 3, 3)
      val capa = 10

      val assigned = solver.rangeInt(items)("item_", 0, 1)

      val profitsVar = items.map(i => assigned(i) * profits(i))

      val weightsVar = items.map(i => assigned(i) * weights(i))

      val totProfit = profitsVar.foldLeft(Sum.zero) {
        (acc, sum) => acc.add(sum)
      }

      val totWeight = weightsVar.foldLeft(Sum.zero) {
        (acc, sum) => acc.add(sum)
      }

      val p = solver int ("profit", 0, profits.sum)
      val w = solver int ("weight", 0, weights.sum)

      //Constraints
      p === totProfit
      w === totWeight
      capa >== w

      solver onSolved {
        println(solver.solution)
      }

      var best = 0

      while (solver solveWith (p >== best + 1)) {
        best = p.value(solver.solution)
      }

      println("finished")
  }

}

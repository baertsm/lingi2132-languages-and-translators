package solver.examples.dsl

import solver.expressions.Term
import solver.dsl.SATSolver
import solver.dsl.SATSolver._

object DSLlatinSquare extends App {

  SATSolver {
    solver =>
      val n = 4
      val range = 0 until n

      val square = Array.tabulate(n * n)(i => {
        solver int ("square " + i, 0, n - 1)
      })

      //constraints
      var terms: IndexedSeq[Term] = IndexedSeq.empty[Term]
      var c = 0

      //For the lines
      for (i <- range) {
        for (j <- range) {
          terms = terms :+ square(j + c)
        }
        allDifferents(terms: _*)
        c += n
        terms = IndexedSeq.empty[Term]
      }

      //columns
      terms = IndexedSeq.empty[Term]
      c = n;
      for (i <- range) {
        for (j <- range) {
          terms = terms :+ square(j * n + i)
        }
        allDifferents(terms: _*)
        terms = IndexedSeq.empty[Term]
      }

      solver onSolved {
        println(solver.solution)
      }

      solver onInfeasible {
        println("Infeasible")
      }

      solver.solve
  }

}

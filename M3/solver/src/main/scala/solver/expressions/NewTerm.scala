package solver.expressions

class NewTerm(term: Term) {

  def ===(t: Term): NewLiteral = {
    NewLiteral(And(LeZero(t - term), LeZero(term - t)))
  }

  def <==(t: Term): NewLiteral = {
    NewLiteral(LeZero(term - t))
  }

  def >==(t: Term): NewLiteral = {
    NewLiteral(LeZero(t - term))
  }

  def <<<(t: Term): NewLiteral = {
    NewLiteral(And(LeZero(term - t), !LeZero(t - term)))
  }

  def >>>(t: Term): NewLiteral = {
    NewLiteral(And(LeZero(t - term), !LeZero(term - t)))
  }

  def !==(t: Term): NewLiteral = {
    NewLiteral(Or(And(LeZero(t - term), !LeZero(term - t)), And(LeZero(term - t), !LeZero(t - term))))
  }

}
package solver.expressions

import solver.dsl.SATSolver

class NewLiteral(literal: Literal) {

  def and(implicit newLiteral: NewLiteral): NewLiteral = {
    /* When a NewLiteral is created, it is directly add in the constraints set
     * of SATSolver.
     * So, before adding a constraint with a 'and', we need to remove the ones
     * that were added when the NewLiteral were created
     */
    SATSolver.constraints -= literal
    SATSolver.constraints -= newLiteral.getLiteral
    NewLiteral(And(literal, newLiteral.getLiteral))
  }

  def or(implicit newLiteral: NewLiteral): NewLiteral = {
    /* When a NewLiteral is created, it is directly add in the constraints set
     * of SATSolver.
     * So, before adding a constraint with a 'or', we need to remove the ones
     * that were added when the NewLiteral were created
     */
    SATSolver.constraints -= literal
    SATSolver.constraints -= newLiteral.getLiteral
    NewLiteral(Or(literal, newLiteral.getLiteral))
  }

  def getLiteral: Literal = literal

}

object NewLiteral {
  def apply(literal: Literal): NewLiteral = {
    /* Add the new constraints defined in the constraints set of SATSolver.
     * Useful to allow the user to directly type his constraints without
     * calling any method of NewSolver
     */
    SATSolver.constraints += literal
    new NewLiteral(literal)
  }
}
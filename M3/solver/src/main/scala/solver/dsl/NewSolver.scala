package solver.dsl

import solver.Solver
import solver.expressions.{ NewLiteral, BooleanVar, Literal, IntVar }
import solver.core.Assignment

class NewSolver(s: Solver) {

  var solvedExc: () => Unit = () => Unit
  var infeasibleExc: () => Unit = () => Unit

  /**
   * Add an IntVar to the solver
   */
  def int(name: String, min: Int, max: Int): IntVar = {
    val iv = new IntVar(name, min, max)
    s.addVariable(iv)
    iv
  }

  /**
   * Add a constraint (represented by a Literal) to the solver
   */
  def constraint(l: Literal) {
    s.addConstraint(l)
  }

  /**
   * Syntactic sugar for the user. Map directly on a range.
   */
  def rangeInt(range: Range)(name: String, min: Int, max: Int): IndexedSeq[IntVar] = {
    range.map(i => {
      this.int(name + i, min, max)
    })
  }

  /**
   * Code executed when a the solve method applied on the solver returned true
   * (The SAT is feasible)
   */
  def onSolved(u: => Unit) {
    solvedExc = () => u
  }

  /**
   * Code executed when a the solve method applied on the solver returned false
   * (The SAT is infeasible)
   */
  def onInfeasible(u: => Unit) {
    infeasibleExc = () => u
  }

  /**
   * Call the solve method of SATSolver in order to add constraints to the
   * solver
   */
  def solve: Boolean = {
    SATSolver.solve
  }

  /**
   * Really call the solve method on the solver and execute the code given by
   * the user for "onSolved" or "OnInfeasible".
   */
  def finish: Boolean = {
    val solvable = s.solve()
    if (solvable) solvedExc()
    else infeasibleExc()
    solvable
  }

  def solution: Assignment = {
    s.solution
  }

  def solveWith(firstConstraint: NewLiteral, otherConstraints: NewLiteral*): Boolean = {
    SATSolver.constraints += firstConstraint.getLiteral
    otherConstraints.map(c => SATSolver.constraints += c.getLiteral)
    SATSolver.solve
  }

}

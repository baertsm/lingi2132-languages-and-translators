package solver.dsl

import solver.expressions._
import solver.Solver

object SATSolver {

  //Set that store the constraints entered by the user. They are added at the end .
  //Used to allow the user to directly type his constraints without calling any method of NewSolver
  var constraints: Set[Literal] = Set.empty

  val solver = new Solver
  val ns = new NewSolver(solver)

  //Implicit conversions
  implicit def term2NewTerm(t: Term) = new NewTerm(t)
  implicit def literal2NewLiteral(l: Literal) = new NewLiteral(l)
  implicit def int2Term(i: Int) = Sum(i)
  implicit def int2NewTerm(i: Int) = new NewTerm(Sum(i))

  //Add constrains to make all the Term given in argument different
  def allDifferents(params: Term*) /*: NewLiteral =*/ {
    val listTerms = params.toList

    for (e0 <- 2 until listTerms.size) {
      for (e1 <- e0 until listTerms.size; if e0 != e1) {
        NewLiteral(different(listTerms(e0), listTerms(e1)))
      }
    }

    def different(e0: Term, e1: Term): Literal =
      Or(And(LeZero(e1 - e0), !LeZero(e0 - e1)),
        And(LeZero(e0 - e1), !LeZero(e1 - e0)))

  }

  //Directly execute the code when called.
  def apply(s: NewSolver => Unit): Unit = {
    s(ns)
  }

  //Add all the constraint to the solver
  def solve: Boolean = {
    for (c <- constraints) {
      ns.constraint(c)
    }
    ns.finish
  }

}


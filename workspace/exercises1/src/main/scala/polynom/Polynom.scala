package polynom

import scala.math

class Polynom(coefs: Traversable[Int]) {

  assert(!coefs.isEmpty, "no coefficient")

  // coefficients(0) + coefficients(1) * x + coefficients(2) * x^2 + ... 
  private val coefficients: Vector[Int] = coefs.toVector

  /**
   * Returns the coefficient related to `degree`.
   * Example: 1 + 2x + 5x^2
   * p(0) = 1
   * p(1) = 2
   * p(5) = 0
   */
  def apply(degree: Int): Int = {
    if (degree < 0) throw new IllegalArgumentException("negative degree")
    else {
      if (degree > this.coefficients.size) {
        return 0;
      } else {
        return this.coefficients(degree);
      }
    }
  }

  /**
   * Returns the product of this `Polynom` with the `Polynom` p.
   * Example: (1 + 2x + 5x^2) * (4x) = 4x + 8x^2 + 20x^3
   */
  def *(p: Polynom): Polynom = {
    val tab = Array.fill(math.max(this.coefficients.size, p.coefficients.size))(0)
    for (i <- 0 to p.coefficients.size) {
      for (j <- 0 to this.coefficients.size) {
        tab(j) = tab(j) + p.coefficients(i) * this.coefficients(j);
      }
    }
    return new Polynom(tab)
  }

  /**
   * Returns the subtraction of this `Polynom` with the `Polynom` p.
   * Example: (1 + 2x + 5x^2) - (4x + x^3) = 1 - 2x + 5x^2 - x^3
   */
  def -(p: Polynom): Polynom = {
    var maxSize = math.max(this.coefficients.size, p.coefficients.size)
    var tab = Array.fill(maxSize)(0)
    for (i <- 0 to this.coefficients.size) {
      tab(i) = tab(i) + this.coefficients(i)
    }
    for (j <- 0 to p.coefficients.size) {
      tab(j) = tab(j) - p.coefficients(j)
    }

    return new Polynom(tab)
  }

  /**
   * Returns the addiction of this `Polynom` with the `Polynom` p
   * Example: (1 + 2x + 5x^2) + (4x + x^3) = 1 + 6x + 5x^2 + x^3
   */
  def +(p: Polynom): Polynom = {
    var maxSize = math.max(this.coefficients.size, p.coefficients.size)
    var tab = new Array[Int](0)
    if (this.coefficients.size < p.coefficients.size) {
      p.coefficients.copyToArray(tab)
      for (i <- 0 to math.min(this.coefficients.size, p.coefficients.size)) {
        tab(i) = tab(i) + this.coefficients(i);
      }
    } else {
      this.coefficients.copyToArray(tab)
      for (i <- 0 to math.min(this.coefficients.size, p.coefficients.size)) {
        tab(i) = tab(i) + p.coefficients(i);
      }
    }

    return new Polynom(tab);
  }

  /**
   * Returns the maximum degree of this `Polynom`.
   * Example: the maximum degree of 4x + x^3 is 3
   */
  def maxDegree: Int = {
    var ret = this.coefficients.size - 1;
    for (i <- this.coefficients.reverseIterator) {
      if (i != 0) return ret
      ret = ret - 1
    }
    return 0
  }

  /**
   * Returns the minimum degree of this `Polynom`.
   * Example: the minimum degree of 1 + 4x + x^3 is 0
   */
  def minDegree: Int = {
    var ret = 0
    for (i <- this.coefficients) {
      if (i != 0) return ret
      ret = ret + 1
    }
    return 0;
  }

  /**
   * Returns true if this `Polynom` represents the same `Polynom` as that.
   */
  override def equals(that: Any) = that match {
    case p: Polynom =>
      if (p.coefficients.size == this.coefficients.size) {
        for (i <- 0 to this.coefficients.size) {
          if (!this.coefficients(i).equals(p.coefficients(i))) {
            false
          }
        }
        true
      } else
        false

    case _ => false
  }

  override def hashCode = coefficients.hashCode

  override def toString: String = {
    coefficients.zipWithIndex.filter(p => p._2 != 0).map(p => p._2 + "x^" + p._1).mkString(" + ")
  }
}

object Polynom {

  // coefs(0) + coefs(1) * x + coefs(2) * x^2 + ...
  def apply(coefs: Int*) = new Polynom(coefs)

  def sum(polynoms: Iterable[Polynom]): Polynom = {
    // hint, uses map/max/min/... to first compute coefficients
    ???
  }
}
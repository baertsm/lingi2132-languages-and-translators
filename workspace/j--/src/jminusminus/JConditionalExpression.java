package jminusminus;

import static jminusminus.CLConstants.GOTO;

/**
 * The AST node for a ternary expression.
 * Inspired of jlhs and jexpression and jassignment
 */
class JConditionalExpression extends JExpression {

    private JExpression lhs;
    private JExpression ifTrue;
    private JExpression ifFalse;

    /**
     * Constructor the AST node for a ternary expression. We will have on
     *
     * @param line line in which the expression occurs in the source file.
     * @param lhs the lhs.
     * @param ifTrue used if lhs is true.
     * @param ifFalse used if lhs is false.
     */
    public JConditionalExpression(int line, JExpression lhs, JExpression ifTrue,
                                  JExpression ifFalse) {
        super(line);
        this.lhs = lhs;
        this.ifTrue = ifTrue;
        this.ifFalse = ifFalse;
    }

    /**
     * Analyze all 3 parts of the ternary expression
     *
     * @param context
     *            context in which names are resolved.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */
    public JExpression analyze(Context context) {
        // a condition (boolean)
        lhs = (JExpression) lhs.analyze(context);
        lhs.type().mustMatchExpected(line(), Type.BOOLEAN);
        // 2 possibilities (expressions)
        ifTrue = (JExpression) ifTrue.analyze(context);
        ifFalse = (JExpression) ifFalse.analyze(context);
        return this;
    }

    /**
     * A ternary expression is a bit like a 'if-else' expression.
     * We need 2 labels: to go to the else (via lhs) and to the end.
     *
     * @param output
     *            the code emitter (basically an abstraction for producing the
     *            .class file).
     */
    public void codegen(CLEmitter output) {
        // the lhs: we need 2 labels
        String elseLabel = output.createLabel();
        String endLabel = output.createLabel();
        lhs.codegen(output, elseLabel, false);
        ifTrue.codegen(output);
        // add branch to the end
        output.addBranchInstruction(GOTO, endLabel);
        output.addLabel(elseLabel);
        ifFalse.codegen(output);
        output.addLabel(endLabel);
    }

    /**
     * a writeToStdOut like the other
     */
    public void writeToStdOut(PrettyPrinter p) {
        // <JConditionalExpression line="X">
        //   <lhs>
        //     (...)
        //   </lhs>
        //   <Assignment>
        //     (...)
        //   </Assignment>
        //   <Condition>
        //     (...)
        //   </Condition>
        // </JConditionalExpression>
        p.printf("<JConditionalExpression line=\"%d\">\n", line());
        p.indentRight();
            p.println("<lhs>");
            p.indentRight();
                lhs.writeToStdOut(p);
            p.indentLeft();
            p.println("</lhs>");
            p.println("<Assignment>");
            p.indentRight();
                ifTrue.writeToStdOut(p);
            p.indentLeft();
            p.println("</Assignment>");
            p.println("<Condition>");
            p.indentRight();
                ifFalse.writeToStdOut(p);
            p.indentLeft();
            p.println("</Condition>");
        p.indentLeft();
        p.println("</JConditionalExpression>");
    }

}

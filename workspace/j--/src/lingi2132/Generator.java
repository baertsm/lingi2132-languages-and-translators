package lingi2132;

import java.util.ArrayList;

import jminusminus.CLEmitter;
import static jminusminus.CLConstants.*;

public class Generator {

    private String outputDir;

    public Generator(String outputDir) {
        this.outputDir = outputDir;
    }

    public void generateClass() {
        CLEmitter output = new CLEmitter(true);

        // array lists
        // public
        ArrayList<String> pub = new ArrayList<String>(1);
        pub.add("public");
        // public static
        ArrayList<String> pubstat = new ArrayList<String>(2);
        pubstat.add("public");
        pubstat.add("static");
 
        // add class declaration
        output.addClass(pub, "packageOfClassToGenerate/ClassToGenerate",
                "java/lang/Object", null, false);

        // add default constructor
        output.addMethod(pub, "<init>", "()V", null, false);
        output.addNoArgInstruction(ALOAD_0);
        output.addMemberAccessInstruction(INVOKESPECIAL, "java/lang/Object",
                "<init>", "()V");
        output.addNoArgInstruction(RETURN);

        // add factorial method declaration
        output.addMethod(pubstat, "factorial", "(I)I", null, false);
        // add factorial body
        // if-else statement
        output.addNoArgInstruction(ILOAD_0);
        output.addBranchInstruction(IFGT, "smaller_than_0"); // if
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(IRETURN);
        output.addLabel("smaller_than_0"); // else
        output.addNoArgInstruction(ILOAD_0);
        output.addNoArgInstruction(ILOAD_0);
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(ISUB);
        output.addMemberAccessInstruction(INVOKESTATIC,
                "packageOfClassToGenerate/ClassToGenerate", "factorial", "(I)I");
        output.addNoArgInstruction(IMUL);
        output.addNoArgInstruction(IRETURN);

        // add maximum method declaration
        output.addMethod(pubstat, "maximum", "([I)I", null, false);
        // add maximum body
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(IALOAD);
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_0);
        output.addNoArgInstruction(IALOAD);
        output.addBranchInstruction(IF_ICMPLE, "smaller"); // if
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(IALOAD);
        output.addNoArgInstruction(IRETURN);
        output.addLabel("smaller"); // else
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_0);
        output.addNoArgInstruction(IALOAD);
        output.addNoArgInstruction(IRETURN);

        // add main method declaration
        output.addMethod(pubstat, "main", "([Ljava/lang/String;)V", null, false);
        // main body
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_0);
        output.addNoArgInstruction(AALOAD);
        output.addMemberAccessInstruction(INVOKESTATIC, "java/lang/Integer",
                "parseInt", "(Ljava/lang/String;)I");
        output.addNoArgInstruction(ISTORE_1);
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(AALOAD);
        output.addMemberAccessInstruction(INVOKESTATIC, "java/lang/Integer",
                "parseInt", "(Ljava/lang/String;)I");
        output.addNoArgInstruction(ISTORE_2);
        output.addNoArgInstruction(ALOAD_0);
        output.addNoArgInstruction(ICONST_2);
        output.addNoArgInstruction(AALOAD);
        output.addMemberAccessInstruction(INVOKESTATIC, "java/lang/Integer",
                "parseInt", "(Ljava/lang/String;)I");
        output.addNoArgInstruction(ISTORE_3);
        output.addNoArgInstruction(ICONST_2);
        output.addArrayInstruction(NEWARRAY, "I");
        output.addOneArgInstruction(ASTORE, 4);
        output.addOneArgInstruction(ALOAD, 4);
        output.addNoArgInstruction(ICONST_0);
        output.addNoArgInstruction(ILOAD_2);
        output.addNoArgInstruction(IASTORE);
        output.addOneArgInstruction(ALOAD, 4);
        output.addNoArgInstruction(ICONST_1);
        output.addNoArgInstruction(ILOAD_3);
        output.addNoArgInstruction(IASTORE);
        output.addMemberAccessInstruction(GETSTATIC, "java/lang/System", "out",
                "Ljava/io/PrintStream;");
        output.addNoArgInstruction(ILOAD_1);
        output.addMemberAccessInstruction(INVOKESTATIC,
                "packageOfClassToGenerate/ClassToGenerate", "factorial", "(I)I");
        output.addMemberAccessInstruction(INVOKEVIRTUAL, "java/io/PrintStream",
                "println", "(I)V");
        output.addMemberAccessInstruction(GETSTATIC, "java/lang/System", "out",
                "Ljava/io/PrintStream;");
        output.addOneArgInstruction(ALOAD, 4);
        output.addMemberAccessInstruction(INVOKESTATIC,
                "packageOfClassToGenerate/ClassToGenerate", "maximum", "([I)I");
        output.addMemberAccessInstruction(INVOKEVIRTUAL, "java/io/PrintStream",
                "println", "(I)V");
        output.addNoArgInstruction(RETURN);

        // write .class file
        output.destinationDir(outputDir);
        output.write();
    }

    public static void main(String[] args) {
        Generator gen = new Generator(args[0]);
        gen.generateClass();
    }
}

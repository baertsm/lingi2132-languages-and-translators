package pass;

import java.lang.System;

public class MultiLinesComments {

    /* Good 
     * multiline comment ! 
     */ 
    public static void main(String[] args) {
        System.out.println("Hello world !"); 
        /*
        int i = 0;
         */
        /***/
        int/*j*/i = 0;
        int/*
        * *
        **/j = 0;
    }
}
/**
**
**/


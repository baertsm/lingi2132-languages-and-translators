package pass;

public class ForExpression {

    public static void main(String[] args) {
        for(;;);
        for (int i = 0; ! (i > 2); ++i);
        for (i = 0; ! (i > 2); ++i) {}
        for (i = 0; ! (i > 2); ++i) {
            int a = i + 1;
        }
        for (i = 0; i > 2; i = i + 1);
        int a;
        for (i = 0, a = 2; ! (i > 2); ++i);
        for (i = 0, a = 2; ! (i > 2); ++i, a = a - 1);

        int[] b = {0, 1, 2};
        for (int c : b) {
            i += c;
        }
        for (final int j = 0; ! (j > 2); ++j);
        for (final int d : b) {
            i += d;
        }
    }

}

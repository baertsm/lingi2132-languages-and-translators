package pass;

public class ConditionalExpression { 

    public void test() {
        int a = true ? 1 : 2;
        a = false ? a : 1;
        a = (a == 1) ? a : 1;
        a = (a == 1) ? ((a + 1 == 2) ? a : 1) : 1;
    }
}

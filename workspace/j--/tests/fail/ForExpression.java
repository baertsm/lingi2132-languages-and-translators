package fail;

import java.lang.System;

public class ForExpression {

    public static void main(String[] args) {
        for (int i = 0; ++i);
        for (int j = 0; j; j; j;) {}
        for (int a : int b);
        boolean notAnArray;
        for (int iNb : notAnArray);
        for (int k = 0; k > -3; ++k) {
    }
}

package fail;

public class ConditionalExpression {

    public static void main(String[] args) {
        int i = true : 1;
        int j = i ? 1 : 1;
        int k = true : 0 ? 1;
        int l = false ? 0;
        int m = true ? 0 ? 1;
        int n = 1 > 0 : 0 : 0;
    }
}

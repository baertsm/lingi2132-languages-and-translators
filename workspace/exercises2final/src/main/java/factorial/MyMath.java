package factorial;

public class MyMath {

	public static int factorial(int n) {
		while (n > 1) {
			return n * factorial(n - 1);
		}
		return 1;
	}
}

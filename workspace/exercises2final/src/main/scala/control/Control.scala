package control

object Control {

  def loop(r: Range)(body: Int => Unit): UnitExc = {
    new UnitExc(r, body)
  }

  class UnitExc(r: Range, body: Int => Unit) {

    def onException(exc: => Unit) {
      for (i <- r) {
        try {
          body(i)
        } catch {
          case _: Throwable =>
            exc
        }
      }
    }
  }

  /*

  class ExceptionHandler {
    def apply(exc: Unit) {
      exc
    }
    def onException(exc: Unit) = apply(exc)
  }*/

  /*class UnitwithExcept(r: Range, body: Int => Unit) {
  def onException(except: => Unit): Unit = except
  }
  def loop(range: Range)(body: Int => Unit): UnitwithExcept =
    for (i <- range) {
          body(i)        
      }
} */

  /*
    
  TODO: 
    
  Create Scala constructs to make this new control block work
   
  loop(1 to 5) { i =>
    
  } onException {
    
  }   
   
   
   This code iterates over a Scala range, 
   i is the current value if iteration passed as argument to the closure.
   If an exception is thrown, it is catched and the the onException block is executed
   before continuing on next value of the range
   
   Hint: Don't hesitate to create intermediate classes
  
  */

}


package polynom

class Polynom(coefs: Int*) {

  // coefficients(0) + coefficients(1) * x + coefficients(2) * x^2 + ... 
  private val coefficients: Vector[Int] = coefs.reverse.dropWhile(_ == 0).reverse.toVector

  /**
   * Return the coefficient related to this degree.
   * Example: 1+2x+5x^2
   * p(0)=Some(1) p(1)=Some(2) p(10)=0 p(-1)=None
   */
  def apply(i: Int): Option[Int] = {
    if (i > 0 && i < this.coefficients.size) {
      Option(this.coefficients(i))
    } else {
      None
    }
  }

  def -(p: Polynom): Polynom = {
    this + new Polynom(p.coefficients.map(-_): _*)
  }

  def +(p: Polynom): Polynom = {
    val smallP = if (p.coefficients.size > this.coefficients.size) this; else p;
    val bigP = if (p.coefficients.size > this.coefficients.size) p;
    else this;
    var pol: Array[Int] = new Array[Int](bigP.coefficients.size)
    for (i <- 0 until smallP.coefficients.size) {
      pol(i) = p.coefficients(i) + this.coefficients(i)
    }
    for (i <- smallP.coefficients.size until bigP.coefficients.size) {
      pol(i) = bigP.coefficients(i)
    }
    new Polynom(pol.map(+_): _*)
  }

  def maxDegree: Int = {
    var ret = this.coefficients.size - 1;
    for (i <- this.coefficients.reverseIterator) {
      if (i != 0) return ret
      ret = ret - 1
    }
    return 0
  }

  def minDegree: Int = {
    var ret = 0
    for (i <- this.coefficients) {
      if (i != 0) return ret
      ret = ret + 1
    }
    return 0;
  }

  override def hashCode = coefficients.hashCode

  override def equals(other: Any) = other match {
    case that: Polynom => {
      maxDegree == that.maxDegree && (0 until maxDegree).forall(i => coefficients(i) == that.coefficients(i))
    }
    case _ => false
  }

  override def toString(): String = {
    var ret = this.coefficients(0).toString
    for (i <- 1 to this.coefficients.length - 1) {
      ret += " ";
      if (this.coefficients(i) < 0) ret += "- "
      else ret += "+ "
      ret += this.coefficients(i)
    }
    return ret
  }

}

object Polynom {

  def apply(coefs: Int*) = new Polynom(coefs: _*)

  def sum(polynoms: Iterable[Polynom]): Polynom = {
    polynoms.reduce((p1, p2) => p1 + p2)
  }

  object x extends Polynom(0, 1) {
    def ^(i: Int): Polynom = {
      var tab = Array.fill(i + 1)(0);
      tab(i) = 1;
      return new Polynom(tab.map(+_): _*)
    }
  }

  implicit def cst2Polynom(i: Int) = new Polynom(i);

  // define an x object 

  // define implicit convertions for constants  (see test suite)
}


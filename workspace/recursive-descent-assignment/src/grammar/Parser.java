package grammar;

import static grammar.Grammar.*;

import java.text.ParseException;

/**
 * @author pschaus, mbaerts, avermeylen
 */
public class Parser {

    private static final boolean DEBUG = false; // print errors when parsing

    /**
     * Grammar to parse
     * S -> if E then S else S
     * S -> begin S L
     * S -> print E
     * L -> end
     * L -> ; S L
     * E -> num = num
     * 
     * @return true if the input is follows the syntax of the grammar, 
     *         i.e. it corresponds to a valid derivation, false otherwise
     */
    public boolean parse(Integer[] input) {
        // We start with a S at the index 0 and without using try/catch
        int index;
        try {
            index = parseS(input, 0);
        } catch (ParseException e) {
            if (DEBUG) {
                index = e.getErrorOffset();
                System.err.println("Error when parsing: " + e.getMessage()
                        + " (received " + input[index]
                        + " at the index: " + index + ")");
            }
            return false;
        } catch (ArrayIndexOutOfBoundsException e) {
            if (DEBUG) {
                System.err.println("Error when parsing: "
                        + "ArrayIndexOutOfBoundsException");
            }
            return false;
        }
        if (index != input.length) // not fully parsed, input is wrong
            return false;
        return true;
    }

    /**
     * Parse a S state
     * @param input the initial input array
     * @param index the index of the current item
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parseS(Integer[] input, int index) throws ParseException {
        // 3 possibilities, start with: 'if', 'begin', 'print'
        if (input[index] == IF) {
            return parseIF(input, index + 1);
        }
        if (input[index] == BEGIN) {
            return parseBEGIN(input, index + 1);
        }
        if (input[index] == PRINT) {
            return parsePRINT(input, index + 1);
        }
        throw new ParseException("parseS: expected "
                + IF + ", " + BEGIN + " or " + PRINT, index);
    }

    /**
     * Parse an IF statement
     * @param input the initial input array (IF + 1)
     * @param index the index of the current item
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parseIF(Integer[] input, int index) throws ParseException {
        int newIndex = parseE(input, index);
        if (input[newIndex] != THEN)
            throw new ParseException("parseIF: expected " + THEN, newIndex);

        newIndex = parseS(input, ++newIndex);

        if (input[newIndex] != ELSE) // it should have a ELSE
            throw new ParseException("parseIF: expected " + ELSE, newIndex);
        return parseS(input, ++newIndex);
    }

    /**
     * Parse an BEGIN statement: begin S L
     * @param input the initial input array
     * @param index the index of the current item (BEGIN + 1)
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parseBEGIN(Integer[] input, int index) throws ParseException {
        // begin S L
        int newIndex = parseS(input, index);
        return parseL(input, newIndex);
    }


    /**
     * Parse an PRINT statement: print E
     * @param input the initial input array
     * @param index the index of the current item (PRINT + 1)
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parsePRINT(Integer[] input, int index) throws ParseException {
        // print E
        return parseE(input, index);
    }


    /**
     * Parse a E state
     * @param input the initial input array
     * @param index the index of the current item (E + 1)
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parseE(Integer[] input, int index) throws ParseException {
        // NUM + EQ + NUM
        if (input[index] != NUM)
            throw new ParseException("parseE: expected " + NUM, index);
        if (input[index + 1] != EQ)
            throw new ParseException("parseE: expected " + EQ, index + 1);
        if (input[index + 2] != NUM)
            throw new ParseException("parseE: expected " + NUM, index + 2);
        return index + 3;
    }


    /**
     * Parse a L state: L -> end || L -> ; S L
     * @param input the initial input array
     * @param index the index of the current item (L + 1)
     * @return the new index to check
     * @throws ParseException if the grammar is not respected
     */
    private int parseL(Integer[] input, int index) throws ParseException {
        // L -> end || L -> ; S L
        if (input[index] == END)
            return index + 1;
        else if (input[index] == SEMI){
            int newIndex = parseS(input, index + 1);
            return parseL(input, newIndex);
        }
        throw new ParseException("parseL: expected "
                + END + " or " + SEMI, index);
    }
}
